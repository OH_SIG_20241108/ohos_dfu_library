package no.nordicsemi.dfu

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DFUApplication : Application()
