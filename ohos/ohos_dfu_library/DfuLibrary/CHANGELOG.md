### 1.0.0
1. 开发环境：Build Version: 5.0.3.910, built on November 1, 2024
2. 验证环境：HOS 5.0.0.49，HOS next，api版本5.0.2 （14） Canary1
3. 验证设备：P70 + MLD HA20 + PLANET 3 PLUS
4. 提供接口：DfuServiceInitiator，DfuServiceController，DfuServiceListenerHelper，DfuProgressListener
5. 支持zip文件升级，start和abort