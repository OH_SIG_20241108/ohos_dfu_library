# ohos_dfu_library

## 介绍

ohos_dfu_library 提供Nordic蓝牙模块nRF系列的固件升级接口。

本项目主要是OpenHarmony系统下以[Android-DFU-Library]([GitHub - NordicSemiconductor/Android-DFU-Library: Device Firmware Update library and Android app](https://github.com/NordicSemiconductor/Android-DFU-Library))为主要依赖开发，主要接口针对OpenHarmony系统进行合理的适配研发。

## 下载安装

1.安装

```
ohpm install @ohos/ohos_dfu_library
```
OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。




## 使用说明

1.使用样例

```typescript
import { DfuServiceInitiator, DfuServiceController,
  DfuServiceListenerHelper, DfuProgressListener } from '@ohos/ohos_dfu_library'

let starter: DfuServiceInitiator = new DfuServiceInitiator(mac);
hilog.info(DOMAIN, TAG, `DfuServiceInitiator`);
starter.setZipUri(this.fileName);

let progressListener: ProgressListener = new ProgressListener();
progressListener.bootloaderCB = this.bootloaderCB;
progressListener.dfuInitializedCB = this.dfuInitializedCB;
progressListener.uploadingCB = this.uploadingCB;
progressListener.completeCB = this.completeCB;
progressListener.dfuAbortedCB = this.abortCB;
progressListener.deviceConnectedCB = this.abortCB;
DfuServiceListenerHelper.registerProgressListener(progressListener);
this.dfuContorller = starter.start();
```



## 接口说明

DFU Library提供以下四个接口类：

1. DfuServiceInitiator：用来初始Dfu服务，配置Zip文件，开始升级

   ```typescript
   // 构造函数，设置升级对象的MAC地址；
   constructor(deviceAddress: string)；
   // 设置固件zip文件的路径（目前是支持从filepicker里得到的沙箱路径）
   public setZipUri(uri: string): DfuServiceInitiator
   // 开始升级
   public start(): DfuServiceController
   ```

   **注意**：其余接口未调试验证过；

   

2. DfuServiceController：升级控制器，先阶段只支持‘abort’

   ```typescript
   // 终止升级，现在采用emitter发消息至升级服务，服务根据状态值进行退出操作，根据DfuProgressListener回调进行判断
   public abort()
   ```

   

3. DfuServiceListenerHelper：用来注册DfuProgressListener

   ```typescript
   // 注册DfuProgressListener
   public static registerProgressListener(listener: DfuProgressListener)
   ```

   

4. DfuProgressListener：回调类，需用户自己基于DfuProgressListener接口类自行实现

   ```
   export interface DfuProgressListener {
     dispose();
     /**
      * Method called when the DFU service started connecting with the DFU target.
      *
      * @param deviceAddress the target device address.
      */
     onDeviceConnecting(deviceAddress: string);
   
     /**
      * Method called when the service has successfully connected, discovered services and found
      * DFU service on the DFU target.
      *
      * @param deviceAddress the target device address.
      */
     onDeviceConnected(deviceAddress: string);
   
     /**
      * Method called when the DFU process is starting. This includes reading the DFU Version
      * characteristic, sending DFU_START command as well as the Init packet, if set.
      *
      * @param deviceAddress the target device address.
      */
     onDfuProcessStarting(deviceAddress: string);
   
     /**
      * Method called when the DFU process was started and bytes about to be sent.
      *
      * @param deviceAddress the target device address
      */
     onDfuProcessStarted(deviceAddress: string);
   
     /**
      * Method called when the service discovered that the DFU target is in the application mode
      * and must be switched to DFU mode. The switch command will be sent and the DFU process
      * should start again. There will be no {@link #onDeviceDisconnected(String)} event following
      * this call.
      *
      * @param deviceAddress the target device address.
      */
     onEnablingDfuMode(deviceAddress: string);
   
     /**
      * Method called during uploading the firmware. It will not be called twice with the same
      * value of percent, however, in case of small firmware files, some values may be omitted.
      *
      * @param deviceAddress the target device address.
      * @param percent       the current status of upload (0-99).
      * @param speed         the current speed in bytes per millisecond.
      * @param avgSpeed      the average speed in bytes per millisecond
      * @param currentPart   the number pf part being sent. In case the ZIP file contains a Soft Device
      *                      and/or a Bootloader together with the application the SD+BL are sent as part 1,
      *                      then the service starts again and send the application as part 2.
      * @param partsTotal    total number of parts.
      */
     onProgressChanged(deviceAddress: string, percent: number, speed: number, avgSpeed: number,
       currentPart: number, partsTotal: number);
   
     onFirmwareValidating(deviceAddress: string);
   
     onDeviceDisconnecting(deviceAddress: string);
   
     onDeviceDisconnected(deviceAddress: string);
   
     onDfuCompleted(deviceAddress: string);
   
     onDfuAborted(deviceAddress: string);
   
     onError(deviceAddress: string, error: number, errorType: number, message: string);
   }
   ```

   




## 约束与限制
在下述版本验证通过：

-  开发环境：Build Version: 5.0.3.910, built on November 1, 2024
-  验证环境：HOS 5.0.0.49，HOS next，api版本5.0.2 （14） Canary1
-  验证设备：P70 + MLD HA20 + PLANET 3 PLUS





## 目录结构

```
|---- ohos_dfu_library
|     |---- AppScrope  # 示例代码文件夹
|     |---- entry  # 示例代码文件夹
|     |---- DfuLibrary  # 核心库
|           |---- src/main/ets  # 模块代码
|                |---- dfu/   # 模块代码
|                     |---- dist     # 打包文件
|            |---- index.ets          # 入口文件
|            |---- .ohpmignore        # ohpm发布的忽略文件
|            |---- *.json5      # 配置文件
|     |---- README.md  # 安装使用方法
|     |---- README.OpenSource  # 开源说明
|     |---- CHANGELOG.md  # 更新日志
```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos_dfu_library/issues) 给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/ohos_dfu_library/pulls)共建 。

## 开源协议

本项目基于 [BSD License](https://gitee.com/openharmony-sig/ohos_dfu_library/blob/master/LICENSE) ，请自由地享受和参与开源。
