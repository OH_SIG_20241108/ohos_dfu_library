/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DfuBaseService } from './DfuBaseService'
import { ManifestFile } from './internal/Manifest'
import worker, { ThreadWorkerGlobalScope, ErrorEvent } from '@ohos.worker';

export enum WorkCmd {
  WC_START_SERVICE = 1,
  WC_CONNECT,
  WC_DISCONNECT,
  WC_ENABLE_CCCD,
  WC_RESTART_SERVICE,
  WC_SEND_INIT_PACKET,
  WC_READ_CHECKSUM,
  WC_SEND_FRAME_PACKET,
  WC_COMPLETE,
}

export interface WorkerMsg {
  cmd: number;
  workerPort?: ThreadWorkerGlobalScope;
  mfFile?: ManifestFile;
  datBuf?: Uint8Array;
  binBuf?: Uint8Array;
  hexBuf?: Uint8Array;
  // application
  applicationBytes?: Uint8Array;
  applicationInitBytes?: Uint8Array;
  // bootloader
  bootloaderBytes?: Uint8Array;
  systemInitBytes?: Uint8Array;
  // soft device
  softDeviceBytes?: Uint8Array;
  // softdevice&bootloader
  softDeviceAndBootloaderBytes?: Uint8Array;
  currentSource?:Uint8Array;

  applicationSize?: number;
  bootloaderSize?: number;
  softDeviceSize?: number;

  deviceAddress: string;
  deviceName: string;
  disableNotification: boolean;
  startAsForegroundService: boolean;
  mimeType: string;
  fileType: number;
  fileUri: string;
  filePath: string;
  fileResId: number;
  initFileUri: string;
  initFilePath: string;
  initFileResId: number;
  keepBond: boolean;
  restoreBond: boolean;
  forceDfu: boolean;
  forceScanningForNewAddressInLegacyDfu: boolean;
  disableResume: boolean;
  numberOfRetries: number;
  mbrSize: number;
  dataObjectDelay: number;
  scanTimeout: number;
  rebootTime: number;
  mtu: number;
  currentMtu: number;
  enableUnsafeExperimentalButtonlessDfu: boolean;
  packetReceiptNotificationsEnabled: boolean;
  numberOfPackets: number;
  legacyDfuUuids: Array<string>;
  secureDfuUuids: Array<string>;
  experimentalButtonlessDfuUuids: Array<string>;
  buttonlessDfuWithoutBondSharingUuids: Array<string>;
  buttonlessDfuWithBondSharingUuids: Array<string>;
}