/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DfuController} from './DfuController'
import { DfuBaseService } from './DfuBaseService'
import hilog from '@ohos.hilog';
import emitter from '@ohos.events.emitter';

export class DfuServiceController implements DfuController {
  private static TAG: string = "DfuServiceController";
  private static DOMAIN: number = 0x8632;

  private mPaused: boolean = false;
  private mAborted: boolean = false;

  DfuServiceController() {
    hilog.info(DfuServiceController.DOMAIN, DfuServiceController.TAG, 'Init DfuServiceController');
  }

  public pause() {
    hilog.info(DfuServiceController.DOMAIN, DfuServiceController.TAG, `dfu pause ${this.mAborted}`);
    if (!this.mAborted && !this.mPaused) {
			this.mPaused = true;
			// final Intent pauseAction = new Intent(DfuBaseService.BROADCAST_ACTION);
			// pauseAction.putExtra(DfuBaseService.EXTRA_ACTION, DfuBaseService.ACTION_PAUSE);
			// mBroadcastManager.sendBroadcast(pauseAction);
      let edata: emitter.EventData = {
        data: {
            "action": DfuBaseService.ACTION_PAUSE,
        }
      };
      emitter.emit("dfuaction", edata);
		}
  }

  public resume() {
    hilog.info(DfuServiceController.DOMAIN, DfuServiceController.TAG, 'dfu resume');
    if (!this.mAborted && this.mPaused) {
			this.mPaused = false;
      let edata: emitter.EventData = {
        data: {
          "action": DfuBaseService.ACTION_RESUME,
        }
      };
      emitter.emit("dfuaction", edata);
			// final Intent pauseAction = new Intent(DfuBaseService.BROADCAST_ACTION);
			// pauseAction.putExtra(DfuBaseService.EXTRA_ACTION, DfuBaseService.ACTION_RESUME);
			// mBroadcastManager.sendBroadcast(pauseAction);
		}
  }

  public abort() {
    hilog.info(DfuServiceController.DOMAIN, DfuServiceController.TAG, `dfu abort ${this.mAborted}`);
    if (!this.mAborted) {
			this.mAborted = true;
			this.mPaused = false;
      let edata: emitter.EventData = {
        data: {
          "action": DfuBaseService.ACTION_ABORT,
        }
      };
      emitter.emit("dfuaction", edata);
			// final Intent pauseAction = new Intent(DfuBaseService.BROADCAST_ACTION);
			// pauseAction.putExtra(DfuBaseService.EXTRA_ACTION, DfuBaseService.ACTION_ABORT);
			// mBroadcastManager.sendBroadcast(pauseAction);
		}
  }

  public isPaused(): boolean {
    return this.mPaused;
  }

  public isAborted(): boolean {
    return this.mAborted;
  }
}