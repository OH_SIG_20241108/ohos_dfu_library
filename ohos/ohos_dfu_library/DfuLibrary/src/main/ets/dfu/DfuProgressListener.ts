/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import hilog from '@ohos.hilog';

export interface DfuProgressListener {
  dispose();
  /**
   * Method called when the DFU service started connecting with the DFU target.
   *
   * @param deviceAddress the target device address.
   */
  onDeviceConnecting(deviceAddress: string);

  /**
   * Method called when the service has successfully connected, discovered services and found
   * DFU service on the DFU target.
   *
   * @param deviceAddress the target device address.
   */
  onDeviceConnected(deviceAddress: string);

  /**
   * Method called when the DFU process is starting. This includes reading the DFU Version
   * characteristic, sending DFU_START command as well as the Init packet, if set.
   *
   * @param deviceAddress the target device address.
   */
  onDfuProcessStarting(deviceAddress: string);

  /**
   * Method called when the DFU process was started and bytes about to be sent.
   *
   * @param deviceAddress the target device address
   */
  onDfuProcessStarted(deviceAddress: string);

  /**
   * Method called when the service discovered that the DFU target is in the application mode
   * and must be switched to DFU mode. The switch command will be sent and the DFU process
   * should start again. There will be no {@link #onDeviceDisconnected(String)} event following
   * this call.
   *
   * @param deviceAddress the target device address.
   */
  onEnablingDfuMode(deviceAddress: string);

  /**
   * Method called during uploading the firmware. It will not be called twice with the same
   * value of percent, however, in case of small firmware files, some values may be omitted.
   *
   * @param deviceAddress the target device address.
   * @param percent       the current status of upload (0-99).
   * @param speed         the current speed in bytes per millisecond.
   * @param avgSpeed      the average speed in bytes per millisecond
   * @param currentPart   the number pf part being sent. In case the ZIP file contains a Soft Device
   *                      and/or a Bootloader together with the application the SD+BL are sent as part 1,
   *                      then the service starts again and send the application as part 2.
   * @param partsTotal    total number of parts.
   */
  onProgressChanged(deviceAddress: string, percent: number, speed: number, avgSpeed: number,
    currentPart: number, partsTotal: number);

  /**
   * Method called when the new firmware is being validated on the target device.
   *
   * @param deviceAddress the target device address.
   */
  onFirmwareValidating(deviceAddress: string);

  /**
   * Method called when the service started to disconnect from the target device.
   *
   * @param deviceAddress the target device address.
   */
  onDeviceDisconnecting(deviceAddress: string);

  /**
   * Method called when the service disconnected from the device. The device has been reset.
   *
   * @param deviceAddress the target device address.
   */
  onDeviceDisconnected(deviceAddress: string);

  /**
   * Method called when the DFU process succeeded.
   *
   * @param deviceAddress the target device address.
   */
  onDfuCompleted(deviceAddress: string);

  /**
   * Method called when the DFU process has been aborted.
   *
   * @param deviceAddress the target device address.
   */
  onDfuAborted(deviceAddress: string);

  /**
   * Method called when an error occur.
   *
   * @param deviceAddress the target device address.
   * @param error         error number.
   * @param errorType     the error type, one of
   *                      {@link DfuBaseService#ERROR_TYPE_COMMUNICATION_STATE},
   *                      {@link DfuBaseService#ERROR_TYPE_COMMUNICATION},
   *                      {@link DfuBaseService#ERROR_TYPE_DFU_REMOTE},
   *                      {@link DfuBaseService#ERROR_TYPE_OTHER}.
   * @param message       the error message.
   */
  onError(deviceAddress: string, error: number, errorType: number, message: string);
}
