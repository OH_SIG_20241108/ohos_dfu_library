/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
import { DfuBaseService } from '../DfuBaseService'
import { MinizipNative } from '@ohos/minizip'
import { util, buffer } from '@kit.ArkTS';
import worker, { MessageEvents, ErrorEvent } from '@ohos.worker';
import { Manifest, FileInfo, ManifestFile } from '../internal/Manifest'
import { WorkerMsg, WorkCmd } from '../WorkMsg';
import hilog from '@ohos.hilog';
import fs from '@ohos.file.fs';
import { ArchiveInputStream } from '../internal/ArchiveInputStream';

// 创建worker线程中与主线程通信的对象
const workerPort = worker.workerPort

const TAG = 'DfuWorker'
const DOMAIN = 0x8632;

const dfuService: DfuBaseService = new DfuBaseService();

function unzip(wmsg: WorkerMsg): WorkerMsg {
  let file = fs.openSync(wmsg.fileUri, fs.OpenMode.READ_ONLY);
  hilog.info(DOMAIN, TAG, `openReadSync: get fd success. file ${JSON.stringify(file)}`);
  hilog.info(DOMAIN, TAG, `openReadSync: get fd success. fd ${JSON.stringify(file.fd)}`);
  let sta = fs.statSync(file.fd);
  hilog.info(DOMAIN, TAG, `fstate: ${JSON.stringify(sta)},${sta.size},${sta.ctime}`);

  let minizipEntry = new MinizipNative(file.path);
  let ret = minizipEntry.Open();
  if (ret == 0) {
    let entryNames = minizipEntry.GetEntryNames();
    for (let i = 0; i < entryNames.length; i++) {
      if(!entryNames[i].endsWith("/")) {
        hilog.info(DOMAIN, TAG, `entryName: ${entryNames[i]}`)
        let arrBuffer = minizipEntry.ExtractFileToJS(entryNames[i], '');
        if (arrBuffer) {
          let ubuf = new Uint8Array(arrBuffer);
          hilog.info(DOMAIN, TAG, `Minizip ${entryNames[i]}: ${ubuf.byteLength}`);

          switch (entryNames[i]) {
            case ArchiveInputStream.APPLICATION_HEX:
              wmsg.applicationBytes = ubuf;
              break;
            case ArchiveInputStream.APPLICATION_BIN:
              wmsg.applicationSize = ubuf.length;
              wmsg.applicationBytes = ubuf;
              break;
            case ArchiveInputStream.APPLICATION_INIT:
              wmsg.applicationInitBytes = ubuf;
              break;
            case ArchiveInputStream.BOOTLOADER_HEX:
              wmsg.bootloaderBytes = ubuf;
              break;
            case ArchiveInputStream.BOOTLOADER_BIN:
              wmsg.bootloaderSize = ubuf.length;
              wmsg.bootloaderBytes = ubuf;
              break;
            case ArchiveInputStream.SOFTDEVICE_HEX:
              wmsg.softDeviceBytes = ubuf;
              break;
            case ArchiveInputStream.SOFTDEVICE_BIN:
              wmsg.softDeviceSize = ubuf.length;
              wmsg.softDeviceBytes = ubuf;
              break;
            case ArchiveInputStream.SYSTEM_INIT:
              wmsg.systemInitBytes = ubuf;
              break;
            default:
              hilog.info(DOMAIN, TAG, 'Not find exactly name');
              break;
          }
          if (entryNames[i].toLowerCase().endsWith('dat')) {
            wmsg.datBuf = ubuf;
          } else if (entryNames[i].toLowerCase().endsWith('bin')) {
            wmsg.binBuf = ubuf;
          } else if (entryNames[i].toLowerCase().endsWith('hex')) {
            wmsg.hexBuf = ubuf;
          }
        }
        if (entryNames[i] == ArchiveInputStream.MANIFEST) {
          let decoder = util.TextDecoder.create('utf-8');
          let str = decoder.decodeToString(new Uint8Array(arrBuffer));
          const manbj: ManifestFile = JSON.parse(str);
          wmsg.mfFile = manbj;
        }
      }
    }
    // {"manifest":{"application":{"bin_file":"nrf52832_HA_App.bin","dat_file":"nrf52832_HA_App.dat"}}}
    if (wmsg.mfFile && wmsg.mfFile.manifest.application) {
      hilog.info(DOMAIN, TAG, `Manifest application: ${JSON.stringify(wmsg.mfFile.manifest.application)}`);
      if (wmsg.binBuf && wmsg.datBuf) {
        wmsg.applicationBytes = wmsg.binBuf;
        wmsg.applicationInitBytes = wmsg.datBuf;
        wmsg.applicationSize = wmsg.binBuf.length;
        hilog.info(DOMAIN, TAG, `manifest bin:${wmsg.applicationSize}, dat:${wmsg.applicationInitBytes.length}`);
      } else {
        throw new Error(`Application file ${JSON.stringify(wmsg.mfFile.manifest.application)} not found.`);
      }
    }
    if (wmsg.mfFile && wmsg.mfFile.manifest.bootloader) {
      hilog.info(DOMAIN, TAG, `Manifest bootloader: ${JSON.stringify(wmsg.mfFile.manifest.bootloader)}`);
      if (wmsg.binBuf && wmsg.datBuf) {
        wmsg.bootloaderBytes = wmsg.binBuf;
        wmsg.systemInitBytes = wmsg.datBuf;
        wmsg.bootloaderSize = wmsg.binBuf.length;
      } else {
        throw new Error(`Bootloader file ${JSON.stringify(wmsg.mfFile.manifest.bootloader)} not found.`);
      }
    }
    if (wmsg.mfFile && wmsg.mfFile.manifest.softdevice) {
      hilog.info(DOMAIN, TAG, `Manifest softdevice: ${JSON.stringify(wmsg.mfFile.manifest.softdevice)}`);
      if (wmsg.binBuf && wmsg.datBuf) {
        wmsg.softDeviceBytes = wmsg.binBuf;
        wmsg.systemInitBytes = wmsg.datBuf;
        wmsg.softDeviceSize = wmsg.binBuf.length;
      } else {
        throw new Error(`SoftDevice file ${JSON.stringify(wmsg.mfFile.manifest.softdevice)} not found.`);
      }
    }
    if (wmsg.mfFile && wmsg.mfFile.manifest.softdeviceBootloader) {
      hilog.info(DOMAIN, TAG,
        `Manifest softdeviceBootloader: ${JSON.stringify(wmsg.mfFile.manifest.softdeviceBootloader)}`);
      if (wmsg.binBuf && wmsg.datBuf) {
        wmsg.softDeviceAndBootloaderBytes = wmsg.binBuf;
        wmsg.systemInitBytes = wmsg.datBuf;
        wmsg.softDeviceSize = wmsg.mfFile.manifest.softdeviceBootloader.sd_size;
        wmsg.bootloaderSize = wmsg.mfFile.manifest.softdeviceBootloader.bl_size;
      } else {
        throw new Error(`softdeviceBootloader ${JSON.stringify(wmsg.mfFile.manifest.softdeviceBootloader)} not found.`);
      }
    }

    //TODO: compatibility mode: manifest.json not exist
    //In this case the ZIP file must contains one or more following files

  } else {
    hilog.info(DOMAIN, TAG, `minizipEntry: ${ret}`);
  }
  return wmsg;
}

// worker线程接收主线程信息
workerPort.onmessage = (e: MessageEvents): void => {
  // data：主线程发送的信息
  let data: string = e.data;
  hilog.info(DOMAIN, TAG, `worker onmessage: ${data}`);

  let wmsg: WorkerMsg = JSON.parse(data);
  if (!wmsg) {
    hilog.error(DOMAIN, TAG, 'workermsg not found');
    return;
  }
  switch (wmsg.cmd) {
    case WorkCmd.WC_START_SERVICE:
      hilog.info(DOMAIN, TAG, `WorkCmd.WC_START_SERVICE: ${wmsg.deviceAddress}`);
      hilog.info(DOMAIN, TAG, `unzip uri: ${wmsg.fileUri}}`);
      if (wmsg.fileUri && wmsg.fileUri.toLowerCase().endsWith('zip')) {
        wmsg = unzip(wmsg);
      }
      dfuService.onHandle(wmsg);
      wmsg.workerPort = workerPort;
      hilog.info(DOMAIN, TAG, 'WC_START_SERVICE end');
      // for (let i=0;i<100000000;i++) {
      //   console.info(`test ${i}`);
      // }
      break;
    case WorkCmd.WC_CONNECT:
      break;
    case WorkCmd.WC_RESTART_SERVICE:
      hilog.info(DOMAIN, TAG, `WorkCmd.WC_RESTART_SERVICE: ${wmsg.deviceAddress}`);
      hilog.info(DOMAIN, TAG, `unzip uri: ${wmsg.fileUri}}`);
      if (wmsg.fileUri && wmsg.fileUri.toLowerCase().endsWith('zip')) {
        wmsg = unzip(wmsg);
      }
      dfuService.onHandle(wmsg);
      wmsg.workerPort = workerPort;
      hilog.info(DOMAIN, TAG, 'WC_RESTART_SERVICE end');
      break;
    default:
      break;
  }
  // worker线程向主线程发送信息
  // workerPort.postMessage("123");
}

// worker线程发生error的回调
workerPort.onerror = (err: ErrorEvent) => {
  hilog.info(DOMAIN, TAG, "worker.ets onerror" + err.message);
}