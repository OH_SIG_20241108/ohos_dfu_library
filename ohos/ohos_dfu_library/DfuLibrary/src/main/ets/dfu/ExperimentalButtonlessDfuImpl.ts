/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ButtonlessDfuImpl } from './ButtonlessDfuImpl'
import { DfuBaseService } from './DfuBaseService'
import { SecureDfuImpl } from './SecureDfuImpl'
import { BaseDfuImpl } from './BaseDfuImpl'
import ble from '@ohos.bluetooth.ble';
import hilog from '@ohos.hilog';

export class ExperimentalButtonlessDfuImpl extends ButtonlessDfuImpl {
  /** The UUID of the experimental Buttonless DFU service from SDK 12.x. */
  static DEFAULT_EXPERIMENTAL_BUTTONLESS_DFU_SERVICE_UUID: string = '8E400001-F315-4F60-9FB8-838830DAEA50';
  /** The UUID of the experimental Buttonless DFU characteristic from SDK 12.x. */
  static DEFAULT_EXPERIMENTAL_BUTTONLESS_DFU_UUID: string = '0x8E400001-F315-4F60-9FB8-838830DAEA50'; // the same as service

  static EXPERIMENTAL_BUTTONLESS_DFU_SERVICE_UUID: string =
    ExperimentalButtonlessDfuImpl.DEFAULT_EXPERIMENTAL_BUTTONLESS_DFU_SERVICE_UUID;
  static EXPERIMENTAL_BUTTONLESS_DFU_UUID: string =
    ExperimentalButtonlessDfuImpl.DEFAULT_EXPERIMENTAL_BUTTONLESS_DFU_UUID;

  private mButtonlessDfuCharacteristic: ble.BLECharacteristic;

  constructor(service: DfuBaseService) {
    super(service);
    this.TAG = 'ExperimentalButtonlessDfuImpl';
  }

  public async isClientCompatible(gattServiceList: Array<ble.GattService>): Promise<boolean> {
    //TODO: find service with spec UUID
    for (const gsItem of gattServiceList) {
      if (gsItem.serviceUuid === ExperimentalButtonlessDfuImpl.EXPERIMENTAL_BUTTONLESS_DFU_SERVICE_UUID) {
        for (const gcItem of gsItem.characteristics) {
          if (gcItem.characteristicUuid === ExperimentalButtonlessDfuImpl.EXPERIMENTAL_BUTTONLESS_DFU_UUID) {
            for (const gdItem of gcItem.descriptors) {
              if (gdItem.descriptorUuid === ExperimentalButtonlessDfuImpl.CLIENT_CHARACTERISTIC_CONFIG.toUpperCase()) {
                this.mButtonlessDfuCharacteristic = gcItem;
                hilog.info(this.DOMAIN, this.TAG, 'Is ExperimentalButtonlessDfuImpl Client');
                return true;
              }
            }
          }
        }
      }
    }
  }

  protected getResponseType(): number {
    return BaseDfuImpl.NOTIFICATIONS;
  }

  protected getButtonlessDfuCharacteristic(): ble.BLECharacteristic {
    return this.mButtonlessDfuCharacteristic;
  }

  protected getDfuServiceUUID(): string {
    return SecureDfuImpl.DFU_SERVICE_UUID;
  }

  public shouldScanForBootloader(): boolean {
    return true;
  }

  public async performDfu() {
    hilog.info(this.DOMAIN, this.TAG, 'ExperimentalButtonlessDfuImpl performDfu');
  }
}