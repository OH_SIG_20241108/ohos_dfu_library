/*
 * Copyright (c) 2024 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DfuBaseService } from '../DfuBaseService'

export class SecureDfuError {
	// DFU status values
	// public static final int SUCCESS = 1; // that's not an error
	public static OP_CODE_NOT_SUPPORTED: number = 2;
	public static INVALID_PARAM: number = 3;
	public static INSUFFICIENT_RESOURCES: number = 4;
	public static INVALID_OBJECT: number = 5;
	public static UNSUPPORTED_TYPE: number = 7;
	public static OPERATION_NOT_PERMITTED: number = 8;
	public static OPERATION_FAILED: number = 10; // 0xA
	public static EXTENDED_ERROR: number = 11; // 0xB

	// public static EXT_ERROR_NO_ERROR = 0x00; // that's not an error
	public static EXT_ERROR_WRONG_COMMAND_FORMAT: number = 0x02;
	public static EXT_ERROR_UNKNOWN_COMMAND: number = 0x03;
	public static EXT_ERROR_INIT_COMMAND_INVALID: number = 0x04;
	public static EXT_ERROR_FW_VERSION_FAILURE: number = 0x05;
	public static EXT_ERROR_HW_VERSION_FAILURE: number = 0x06;
	public static EXT_ERROR_SD_VERSION_FAILURE: number = 0x07;
	public static EXT_ERROR_SIGNATURE_MISSING: number = 0x08;
	public static EXT_ERROR_WRONG_HASH_TYPE: number = 0x09;
	public static EXT_ERROR_HASH_FAILED: number = 0x0A;
	public static EXT_ERROR_WRONG_SIGNATURE_TYPE: number = 0x0B;
	public static EXT_ERROR_VERIFICATION_FAILED: number = 0x0C;
	public static EXT_ERROR_INSUFFICIENT_SPACE: number = 0x0D;

	// public static BUTTONLESS_SUCCESS = 1;
	public static BUTTONLESS_ERROR_OP_CODE_NOT_SUPPORTED: number = 2;
	public static BUTTONLESS_ERROR_OPERATION_FAILED: number = 4;

	public static parse(error: number): string {
		switch (error) {
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.OP_CODE_NOT_SUPPORTED:
				return "OP CODE NOT SUPPORTED";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.INVALID_PARAM:
				return "INVALID PARAM";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.INSUFFICIENT_RESOURCES:
				return "INSUFFICIENT RESOURCES";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.INVALID_OBJECT:
				return "INVALID OBJECT";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.UNSUPPORTED_TYPE:
				return "UNSUPPORTED TYPE";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.OPERATION_NOT_PERMITTED:
				return "OPERATION NOT PERMITTED";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.OPERATION_FAILED:
				return "OPERATION FAILED";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE | SecureDfuError.EXTENDED_ERROR:
				return "EXTENDED ERROR";
			default:
				return "UNKNOWN (" + error + ")";
		}
	}

	public static parseExtendedError(error: number): string {
		switch (error) {
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_WRONG_COMMAND_FORMAT:
				return "Wrong command format";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_UNKNOWN_COMMAND:
				return "Unknown command";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_INIT_COMMAND_INVALID:
				return "Init command invalid";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_FW_VERSION_FAILURE:
				return "FW version failure";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_HW_VERSION_FAILURE:
				return "HW version failure";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_SD_VERSION_FAILURE:
				return "SD version failure";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_SIGNATURE_MISSING:
				return "Signature mismatch";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_WRONG_HASH_TYPE:
				return "Wrong hash type";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_HASH_FAILED:
				return "Hash failed";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_WRONG_SIGNATURE_TYPE:
				return "Wrong signature type";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_VERIFICATION_FAILED:
				return "Verification failed";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_EXTENDED | SecureDfuError.EXT_ERROR_INSUFFICIENT_SPACE:
				return "Insufficient space";
			default:
				return "Reserved for future use";
		}
	}

	public static parseButtonlessError(error: number): string {
		switch (error) {
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_BUTTONLESS | SecureDfuError.BUTTONLESS_ERROR_OP_CODE_NOT_SUPPORTED:
				return "OP CODE NOT SUPPORTED";
			case DfuBaseService.ERROR_REMOTE_TYPE_SECURE_BUTTONLESS | SecureDfuError.BUTTONLESS_ERROR_OPERATION_FAILED:
				return "OPERATION FAILED";
			default:
				return "UNKNOWN (" + error + ")";
		}
	}
}