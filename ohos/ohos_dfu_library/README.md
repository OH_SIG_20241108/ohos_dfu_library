# OHOS DFU Application

本软件是参考Nordic DFU（Device Firmware Update） SDK （https://github.com/NordicSemiconductor/Android-DFU-Library） 开发出的 OpenHarmony 应用，包括DFU升级应用和DFU SDK。此应用特点为支持以zip包的形式加载文件，然后通过移动设备（如手机，平板）的蓝牙模块发送到 nRF5 蓝牙模块并完成蓝牙模块升级的功能。



#### 界面

![show](.\figures\show.jpg)

以上就是现在设备固件升级工具的实际运行效果，目前版本支持沐澜的MLD HA20和捷安特的Planet的设备。Nordic的nRF蓝牙模块本来支持两种协议：Legacy协议和Secure协议。但是现在只找到两种设备，并且都是Secure协议，所以目前在选择设备阶段只支持两种设备类型。

**注意**

* 不同于Android设备，HOS现在在连接蓝牙设备并使其进入Bootloader模式后，应为设备重启，会重新发现蓝牙设备，并改变蓝牙设备地址（随机MAC）导致目前蓝牙设备是通过名称进行继续发现和固件上传的。Android设备重新发现地址不会改变（只是最末位MAC地址+1，HOS设备重新发现时，对应的MAC会全随机）；
* 不同于Android开发，HOS没有lock机制，所以目前开发没有支持暂停功能；
* 依赖'@ohos/minizip'三方库对zip文件进行内存解压的对应文件内容读取；
* 使用emitter进行进程内的消息通知，支持DfuProgressListener功能；



#### 使用

DFU Library提供以下四个接口类：

1. DfuServiceInitiator：用来初始Dfu服务，配置Zip文件，开始升级

   ```typescript
   // 构造函数，设置升级对象的MAC地址；
   constructor(deviceAddress: string)；
   // 设置固件zip文件的路径（目前是支持从filepicker里得到的沙箱路径）
   public setZipUri(uri: string): DfuServiceInitiator
   // 开始升级
   public start(): DfuServiceController
   ```

   **注意**：其余接口未调试验证过；

   

2. DfuServiceController：升级控制器，先阶段只支持‘abort’

   ```typescript
   // 终止升级，现在采用emitter发消息至升级服务，服务根据状态值进行退出操作，根据DfuProgressListener回调进行判断
   public abort()
   ```

   

3. DfuServiceListenerHelper：用来注册DfuProgressListener

   ```typescript
   // 注册DfuProgressListener
   public static registerProgressListener(listener: DfuProgressListener)
   ```

   

4. DfuProgressListener：回调类，需用户自己基于DfuProgressListener接口类自行实现

   ```
   export interface DfuProgressListener {
     dispose();
     /**
      * Method called when the DFU service started connecting with the DFU target.
      *
      * @param deviceAddress the target device address.
      */
     onDeviceConnecting(deviceAddress: string);
   
     /**
      * Method called when the service has successfully connected, discovered services and found
      * DFU service on the DFU target.
      *
      * @param deviceAddress the target device address.
      */
     onDeviceConnected(deviceAddress: string);
   
     /**
      * Method called when the DFU process is starting. This includes reading the DFU Version
      * characteristic, sending DFU_START command as well as the Init packet, if set.
      *
      * @param deviceAddress the target device address.
      */
     onDfuProcessStarting(deviceAddress: string);
   
     /**
      * Method called when the DFU process was started and bytes about to be sent.
      *
      * @param deviceAddress the target device address
      */
     onDfuProcessStarted(deviceAddress: string);
   
     /**
      * Method called when the service discovered that the DFU target is in the application mode
      * and must be switched to DFU mode. The switch command will be sent and the DFU process
      * should start again. There will be no {@link #onDeviceDisconnected(String)} event following
      * this call.
      *
      * @param deviceAddress the target device address.
      */
     onEnablingDfuMode(deviceAddress: string);
   
     /**
      * Method called during uploading the firmware. It will not be called twice with the same
      * value of percent, however, in case of small firmware files, some values may be omitted.
      *
      * @param deviceAddress the target device address.
      * @param percent       the current status of upload (0-99).
      * @param speed         the current speed in bytes per millisecond.
      * @param avgSpeed      the average speed in bytes per millisecond
      * @param currentPart   the number pf part being sent. In case the ZIP file contains a Soft Device
      *                      and/or a Bootloader together with the application the SD+BL are sent as part 1,
      *                      then the service starts again and send the application as part 2.
      * @param partsTotal    total number of parts.
      */
     onProgressChanged(deviceAddress: string, percent: number, speed: number, avgSpeed: number,
       currentPart: number, partsTotal: number);
   
     onFirmwareValidating(deviceAddress: string);
   
     onDeviceDisconnecting(deviceAddress: string);
   
     onDeviceDisconnected(deviceAddress: string);
   
     onDfuCompleted(deviceAddress: string);
   
     onDfuAborted(deviceAddress: string);
   
     onError(deviceAddress: string, error: number, errorType: number, message: string);
   }
   ```

   

##### 简单样例：

```typescript
let starter: DfuServiceInitiator = new DfuServiceInitiator(mac);
hilog.info(DOMAIN, TAG, `DfuServiceInitiator`);
starter.setZipUri(this.fileName);

let progressListener: ProgressListener = new ProgressListener();
progressListener.bootloaderCB = this.bootloaderCB;
progressListener.dfuInitializedCB = this.dfuInitializedCB;
progressListener.uploadingCB = this.uploadingCB;
progressListener.completeCB = this.completeCB;
progressListener.dfuAbortedCB = this.abortCB;
progressListener.deviceConnectedCB = this.abortCB;
DfuServiceListenerHelper.registerProgressListener(progressListener);
this.dfuContorller = starter.start();
```



#### 环境验证

1. 开发环境：Build Version: 5.0.3.910, built on November 1, 2024
2. 验证环境：HOS 5.0.0.49，HOS next，api版本5.0.2 （14） Canary1
3. 验证设备：P70 + MLD HA20 + PLANET 3 PLUS



#### 最后

如有需求请提ISSUE，会在7个工作日内回复。